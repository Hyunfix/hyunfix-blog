---
title: Présentation de Hyunfix
date: 2022-09-03T18:32:11.756Z
draft: false
description: apprendre à me connaitre
tags:
  - java
  - dev
  - spring
slug: about-me-blog
lastmod: 2022-09-03T20:23:33.674Z
---
# Qui-suis je ? 
Je me présente mon nom est Alexandre et j'ai 26 ans. Cela fait 4 ans et demi que je suis développeur Java dans une ESN en région parisienne. Je souhaite monter en compétences en angular et d'autres languages, mais je souhaite apporter à mon aide à tout développeur dans le besoin. 

## Ma Stack technique:

* Java
* Spring/Spring boot
* Oracle/PostgreSQL
* Kafka/IBM MQ
* Docker